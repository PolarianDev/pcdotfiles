# History:
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Promt
autoload -Uz promptinit
promptinit
prompt spaceship

# Local servers
alias rpi="ssh rpi.polaris"
alias chromebox="ssh chromebox.polaris"

# git
alias push="git push"
alias pull="git pull"
alias add="git add ."
alias status="git status"

alias suspend="doas systemctl suspend"
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

export PATH=/usr/share/dotnet:$PATH
export DOTNET_CLI_TELEMETRY_OPTOUT=true

export JDTLS_HOME=~/jdtls/
export WORKSPACE=~/workspaces/
